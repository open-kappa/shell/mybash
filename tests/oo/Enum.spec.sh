#!/usr/bin/env bash


myBash.test.module "myBash.oo.Enum"

myBash.test.section "Base testing"

myBash.test.addTest "Declare enum" declareEnum
function declareEnum()
{
    enum Test \
        KEY_ONLY \
        KEY_AND_STRING = "String value"
        KEY_AND_NUMBER = 42
    myBash.test.assert.exists(Test)
}

myBash.test.addTest "Check: no value" checkNoValue

myBash.test.addTest "Check: string value" checkStringValue

myBash.test.addTest "Check: number value" checkNumberValue

# EOF
