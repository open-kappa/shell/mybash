#!/usr/bin/env bash

## Check if the given function exists.
## It can be either a custom or builtin function or a program file.
## @param NAME the name to check.
## @return 0 if exists, 1 otherwise
function checkExists()
{
    local NAME="$1"
    local OUT="$2"
    local ANSWER=$(type -t $NAME)
    local RES="0"
    case "$ANSWER" in
        "alias")
            RES="1"
            ;;
        "keyword")
            RES="1"
            ;;
        "function")
            RES="0"
            ;;
        "builtin")
            RES="0"
            ;;
        "file")
            RES="0"
            ;;
        "")
            RES="1"
            ;;
        *)
            # Unexpected...
            RES="1"
            ;;
    esac
    return $RES
}

# EOF
