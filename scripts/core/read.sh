#!/usr/bin/env bash

## Read a yes/no input.
## @param MSG The message to show.
## @param DEFAULT Optional. Default answer. It can be "--yes" or "--no"
## @param OUT The output variable: 0 if "yes", 1 if "no".
function readYesNo()
{
    local MSG="$1"
    local DEFAULT="$2"
    local OUT="$3"
    local RES=""
    if [ "$OUT" == "" ]; then
        OUT="$DEFAULT"
        DEFAULT=""
    fi
    while [ "1" == "1" ]; do
        if [ "$DEFAULT" = "--yes" ]; then
            echo -n "$MSG [Y/n] "
        elif [ "$DEFAULT" = "--no" ]; then
            echo -n "$MSG [y/N] "
        else
            echo -n "$MSG [y/n] "
        fi
        read RES
        if [ "$RES" == "yes" ] || [ "$RES" == "y" ] || [ "$RES" == "Y" ] || [ "$RES" == "YES" ]; then
            eval ${OUT}="0"
            break
        elif [ "$RES" == "no" ] || [ "$RES" == "n" ] || [ "$RES" == "N" ] || [ "$RES" == "NO" ]; then
            eval ${OUT}="1"
            break
        elif [ "$DEFAULT" = "--yes" ] && [ "$RES" == "" ]; then
            eval ${OUT}="0"
            break
        elif [ "$DEFAULT" = "--no" ] && [ "$RES" == "" ]; then
            eval ${OUT}="1"
            break
        else
            echo "Invalid answer"
        fi
    done
}

## Read an input from the user.
## The input cannot be the empty string.
## @param MSG The message to show.
## @param EMPTY Optional. Pass --empty to allow an empty string as result.
## @param OUT The output variable.
function readInput()
{
    local myBash_MSG="$1"
    local myBash_EMPTY="$2"
    local myBash_OUT="$3"
    local myBash_RES=""
    if [ "$myBash_OUT" == "" ]; then
        myBash_OUT="$myBash_EMPTY"
        myBash_EMPTY=""
    fi
    while [ "1" == "1" ]; do
        echo -n "$myBash_MSG "
        read myBash_RES
        if [ "$myBash_RES" == "" ] && [ "$myBash_EMPTY" == "--empty" ]; then
            break;
        if [ "$myBash_RES" == "" ]; then
            echo "Invalid answer"
            continue
        fi
        break
    done
    eval ${myBash_OUT}="$myBash_RES"
}

## Read an input from the user chosen in a list.
## @param MSG The message to show.
## @param EMPTY Optional. Pass --empty to allow an empty string as result.
## @param ... The list of choices.
## @return The index of the choice, starting from 1. Zero means empty string.
function readChoice()
{
    local myBash_MSG="$1"
    local myBash_EMPTY=""
    local myBash_RES=""
    local myBash_i="0"
    local myBash_re='^[0-9]+$'
    shift
    shift
    if [ "$myBash_OUT" == "--empty" ]; then
        myBash_OUT="$3"
        myBash_EMPTY="$2"
        shift
    fi
    while [ "1" == "1" ]; do
        echo "$myBash_MSG"
        for myBash_CH in $@; do
            myBash_i=$((myBash_i + 1))
            echo "$myBash_i - $myBash_CH"
        done
        echo -n "Choice number: "
        read myBash_RES
        if [ "$myBash_RES" == "" ] && [ "$myBash_EMPTY" == "--empty" ]; then
            myBash_RES="0"
            break;
        elif [[ $myBash_RES =~ $myBash_re ]] && ((myBash_RES > 0)) && ((myBash_RES <= myBash_i)); then
            break;
        else
            echo "Invalid answer"
        fi
    done
    return "$myBash_RES"
}

# EOF
