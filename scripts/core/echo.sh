#!/usr/bin/env bash

## Echo an error message on stderr.
## The message is in red, inside terminals.
## No color is used in case of redirect.
## @param $@ The list of strings to print.
function echoError()
{
    if [ -t 2 ]; then
        echo -e "\e[31m"$@"\e[0m" 1>&2
    else
        echo -e $@ 1>&2
    fi
}

## Echo a warning message on stderr.
## The message is in yellow, inside terminals.
## No color is used in case of redirect.
## @param $@ The list of strings to print.
function echoWarn()
{
    if [ -t 2 ]; then
        echo -e "\e[33m"$@"\e[0m" 1>&2
    else
        echo -e $@ 1>&2
    fi
}

## Echo a informational message on stdout.
## The message is in green, inside terminals.
## No color is used in case of redirect.
## @param $@ The list of strings to print.
function echoInfo()
{
    if [ -t 1 ]; then
        echo -e "\e[32m"$@"\e[0m"
    else
        echo -e $@
    fi
}

# EOF
