#!/usr/bin/env bash

function isNaturalNumber()
{
    local TOCHECK="$1"
    local REGEX='^[1-9][0-9]*$'
    if ! [[ $TOCHECK =~ $REGEX ]]; then
        return 1
    fi
    return 0
}

# EOF
