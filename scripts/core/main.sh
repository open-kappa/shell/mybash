#!/usr/bin/env bash

## This package path.
export MYBASH_CORE_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

## Load all MyBash Core functions.
function sourceMyBashCore()
{
    . $MYBASH_CORE_DIR/echo.sh
    . $MYBASH_CORE_DIR/getScriptDir.sh
}

# EOF
