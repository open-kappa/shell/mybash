#!/usr/bin/env bash

## Get the current script directory.
## @param OUT The output variable.
function getScriptDir()
{
    eval ${1}=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
}

# EOF
