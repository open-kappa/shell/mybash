#!/usr/bin/env bash

## MyBash version major.
export MYBASH_VERSION_MAJOR="0"
## MyBash version minor.
export MYBASH_VERSION_MINOR="0"
## MyBash version revision.
export MYBASH_VERSION_REVISION="0"
## MyBash version.
export MYBASH_VERSION="$MYBASH_VERSION_MAJOR.$MYBASH_VERSION_MINOR.$MYBASH_VERSION_REVISION"
## MyBash path.
export MYBASH_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

## Load all MyBash functions.
function myBash()
{
    . $MYBASH_DIR/core/main.sh
    . $MYBASH_DIR/gui/main.sh
}

# EOF
