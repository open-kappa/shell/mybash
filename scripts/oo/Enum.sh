#!/usr/bin/env bash

## Declare a new enum.
## As parameters, expecting a list of `key = value`, or `key`.
## Internally, creates a group of functions, with the name "enumName.key".
## All these functions:
## * "echo" the value if provided, the key otherwise
## * return the value if it is a number, zero otherwise.
## @param enumName The name of the enum type.
## @param ... The list of parameters.
function enum()
{
    declare -r enumName="$1"
    shift
    if [ "$enumName" == "" ]; then
        echoError "Invalid enum: missing name"
        exit 1
    fi

    declare -A keyVal
    declare -a keys
    declare -a vals
    while [[ "$#" != "0" ]]; do
        keys+="$1"
        if [[ "$2" == "=" ]]; then
            keyVal[$1]="$3"
            vals+="$3"
        else
            keyVal[$1]="$1"
            vals+="$1"
        fi
    done

    eval "function $enumName()
    {
        declare -a values=(${vals[@]})
        if [[ \"$1\" == \".\" ]]; then
            eval \"$enumName.\$1()\"
        else
            echo \"${values[@]}\"
        fi
    }"
    declare -fr $enumName

    # while [[ "$#" != "0" ]]; do
    #     if [[ "$2" == "=" ]]; then
    #         if isNaturalNumber $3; then
    #             eval "function $enumName.$1()
    #             {
    #                 echo \"$3\"
    #                 return \"$3\"
    #             }"
    #         else
    #             eval "function $enumName.$1()
    #             {
    #                 echo \"$3\"
    #                 return 0
    #             }"
    #         fi
    #         shift
    #         shift
    #         shift
    #     else
    #         eval "function $enumName.$1()
    #         {
    #             echo \"$1\"
    #             return 0
    #         }"
    #         shift
    #     fi
    # done
}
declare -gfr enum

# EOF
