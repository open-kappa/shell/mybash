#!/usr/bin/env bash

function myBash.errors.OK() {return 0;}
function myBash.errors.MISSING_DIALOG() {return 1;}
function myBash.errors.INVALID_MODE() {return 2;}
function myBash.errors.INVALID_TYPE() {return 3;}
function myBash.errors.INVALID_BOOLEAN_VALUE() {return 4;}
function myBash.errors.MISSING_BATCH_VALUE() {return 5;}

# EOF
