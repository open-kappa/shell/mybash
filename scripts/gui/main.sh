#!/usr/bin/env bash

## This package path.
export MYBASH_GUI_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

## Load all MyBash GUI functions.
function sourceMyBashGUI()
{
    . $MYBASH_GUI_DIR/configureGui.sh
}

# EOF
