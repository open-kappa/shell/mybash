#!/usr/bin/env bash

## Parse the configuration flags.
## @see parseGuiParams() for a description of parameters
## @private
function myBash.gui._parseFlagParams()
{
    while [ "$1" != "" ]; do
        case "$1" in
            "--ui")
                myBash._BEHAVIOR="$2"
                shift
                case "$myBash._BEHAVIOR" in
                    "batch")
                        ;;
                    "gui")
                        if test $(type -t dialog) != "file"
                            myBash.errors.MISSING_DIALOG
                            return $?
                        fi
                        ;;
                    "textual")

                        ;;
                    *)
                        myBash.errors.INVALID_MODE
                        return $?
                        ;;
                esac
                ;;
            "--batch-value")
                myBash_BATCH_NAME="$2"
                myBash_BATCH_TYPE="$3"
                shift
                shift
                case "$myBash_BATCH_TYPE" in
                    "string")
                        ;;
                    "boolean")
                        ;;
                    *)
                        return $MYBASH_ERROR_INVALID_MODE
                        ;;
                ;;
            *)
                # ntd
                ;;
        esac
        shift
    done
    return $MYBASH_ERROR_OK
}

## Parse the configuration values.
## @see parseGuiParams() for a description of parameters
## @private
function _myBash_parseGuiValueParams()
{
    while [ "$1" != "" ]; do
        case "$1" in
            "$myBash_BATCH_NAME")
                myBash_BATCH_VALUE="$2"
                shift
                case "$myBash_BATCH_TYPE" in
                    "string")
                        ;;
                    "flag")
                        case "$myBash_BATCH_VALUE" in
                            "0")
                                myBash_BATCH_VALUE="0"
                                ;;
                            "1")
                                myBash_BATCH_VALUE="1"
                                ;;
                            "true")
                                myBash_BATCH_VALUE="0"
                                ;;
                            "false")
                                myBash_BATCH_VALUE="1"
                                ;;
                            "y")
                                myBash_BATCH_VALUE="0"
                                ;;
                            "n")
                                myBash_BATCH_VALUE="1"
                                ;;
                            "yes")
                                myBash_BATCH_VALUE="0"
                                ;;
                            "no")
                                myBash_BATCH_VALUE="1"
                                ;;
                            "YES")
                                myBash_BATCH_VALUE="0"
                                ;;
                            "NO")
                                myBash_BATCH_VALUE="1"
                                ;;
                            "ON")
                                myBash_BATCH_VALUE="0"
                                ;;
                            "OFF")
                                myBash_BATCH_VALUE="1"
                                ;;
                            *)
                                return $MYBASH_ERROR_INVALID_BOOLEAN_VALUE
                                ;;
                        esac
                        ;;
                    *)
                        return $MYBASH_ERROR_INVALID_TYPE
                        ;;
                esac
                ;;
            *)
                # ntd
                ;;
        esac
        shift
    done
    return 0
}

## Parse a list of parameters, allowed for the GUI.
## Parameters:
## * --ui <value> Force a gui behavior:
##     * "batch": force batch
##     * "gui": force textual gui
##     * "textual": force textual interactions
## * --batch-value <name> <type> The name of the parameter of the batch value.
##     Its type can be:value is a string.
##     * "string": a free string.
##     * "boolean": one of the following: 0, 1, true, false, yes, no, y, n, YES,
##         NO, ON, OFF.
## @param OUT_BEHAVIOR The behavior of the GUI. Default is empty string, meaning
##     graceful degradation.
## @param OUT_BATCH_VALUE The value to use for the batch behavior case. For
##     booleans it can be 0 or 1 or empty string.
## @return 0 on success, or one of MYBASH_ERROR_MISSING_DIALOG,
##     MYBASH_ERROR_INVALID_MODE, MYBASH_ERROR_INVALID_TYPE,
##     MYBASH_ERROR_INVALID_BOOLEAN_VALUE, MYBASH_ERROR_MISSING_BATCH_VALUE
function myBash.parseGuiParams()
{
    local myBash_OUT_BEHAVIOR="$1"
    local myBash_OUT_BATCH_VALUE="$2"
    local myBash_BEHAVIOR=""
    local myBash_BATCH_NAME=""
    local myBash_BATCH_TYPE=""
    local myBash_BATCH_VALUE=""
    local myBash_ERR="$MYBASH_ERROR_OK"
    shift
    shift
    eval myBash_BEHAVIOR="$$$myBash_OUT_BEHAVIOR"
    eval myBash_BATCH_VALUE="$$$myBash_OUT_BATCH_VALUE"

    _myBash_parseGuiFlagParams "$@"
    myBash_ERR="$?"
    if [ "$myBash_ERR" != "0" ]; then
        return $myBash_ERR
    fi
    _myBash_parseGuiValueParams "$@"
    myBash_ERR="$?"
    if [ "$myBash_ERR" != "0" ]; then
        return $myBash_ERR
    fi
    if [ "$myBash_BEHAVIOR" == "batch" ] && [ "$myBash_BATCH_VALUE" == "" ]; then
        return $MYBASH_ERROR_MISSING_BATCH_VALUE
    elif if [ "$myBash_BEHAVIOR" == "" ]; then
        if test $(type -t dialog) == "file"
            myBash_BEHAVIOR="gui"
        else
            myBash_BEHAVIOR="textual"
        fi
    fi

    eval $myBash_OUT_BEHAVIOR="$myBash_BEHAVIOR"
    eval $myBash_OUT_BATCH_VALUE="$myBash_BATCH_VALUE"
    return $MYBASH_ERROR_OK
}

# EOF
