#!/usr/bin/env bash

## Creates a menu.
## @param TITLE The menu title
## @param MSG The menu message
## @param OUT The result.
function Menu()
{
    local GUI_BEHAVIOR="$_MYBASH_GUI_BEHAVIOR"
    local GUI_VALUE="$_MYBASH_GUI_FLAG_VALUE"
    parseGuiParams $@
    if
    if [ "$GUI_BEHAVIOR" == "" ]; then
    fi
    case "$GUI_BEHAVIOR" in
        "batch")
        "gui")
        "textual")
        *)
            ;;
    esac

    $(dialog --clear --stdout --backtitle "Test" --title "Say Hi" --menu "Ester egg"  0 0 0 "1" "Hello world" "2" "Ciao Mondo")
}

# EOF
