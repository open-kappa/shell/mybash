#!/usr/bin/env bash

## Store the GUI behavior.
## Type: "batch"|"gui"|"textual"|""
## @private
export _MYBASH_GUI_BEHAVIOR=""
## Store the default flag value.
## @private
export _MYBASH_GUI_FLAG_VALUE=""

## Configure GUI initial state.
## @see parseGuiParams() for parameters and return values.
function configureGui()
{
    local myBash_ERR="$MYBASH_ERROR_OK"
    parseGuiParams _MYBASH_GUI_BEHAVIOR _MYBASH_GUI_FLAG_VALUE "$@"
    myBash_ERR="$?"
    if [ "$myBash_ERR" != "$MYBASH_ERROR_OK" ]; then
        return $myBash_ERR
    fi
    return $MYBASH_ERROR_OK
}

# EOF
