#!/usr/bin/env bash

## This package path.
declare -xr MYBASH_TEST_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)

## Load all MyBash test functions.
function myBash.test.source()
{
    . $MYBASH_TEST_DIR/runner.sh "$@"
}
declare -fr myBash.test.source

myBash.test.source "$@"

# EOF
