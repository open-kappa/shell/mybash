#!/usr/bin/env bash

## Declare the highest level of test groups
## @param NAME The test module name
function myBash.test.module()
{
    _MYBASH_TEST_MODULE_NAME="$1"
    if [[ $_MYBASH_TEST_ISFIRSTRUN == "1" ]]; then
        declare -A _MYBASH_TEST_SECTIONS
        _MYBASH_TEST_MODULES["$_MYBASH_TEST_MODULE_NAME"]=${_MYBASH_TEST_SECTIONS[@]}
    fi
}
declare -frg myBash.test.module

## Declare a optional test section
## @param NAME The test section name
function myBash.test.section()
{
    _MYBASH_TEST_SECTION_NAME="$1"
    #_MYBASH_TEST_MODULES["$_MYBASH_TEST_MODULE_NAME"]+=
}
declare -fr myBash.test.section

## Declare a test.
## @param name The test name
## @param foo The test function
function myBash.test.addTest()
{
    # ntd
    echo "" > /dev/null
}
declare -fr myBash.test.addTest

## Print the status.
## @private
function myBash.test._printStatus()
{
    local moduleName
    local moduleSections
    for moduleName in ${!_MYBASH_TEST_MODULES[@]}; do
        echo "Module: ${moduleName}"
        moduleSections=${_MYBASH_TEST_MODULES[$moduleName]}
    done
}
declare -fr myBash.test._printStatus


## Main for tests.
## @param DIR The test dir
## @param GREP Optional. Grep expression to filter tests.
function myBash.test.main()
{
    echo "Starting tests..."

    declare -r  DIR="$1"
    declare -r  GREP="$2"
    local _MYBASH_TEST_MODULE_NAME=""
    local _MYBASH_TEST_SECTION_NAME=""
    declare -A _MYBASH_TEST_MODULES
    declare -r _MYBASH_TEST_TEST_NAME="testName"
    declare -r _MYBASH_TEST_TEST_FOO="testFoo"

    local _MYBASH_TEST_ISFIRSTRUN="1"

    local _MYBASH_TEST_TESTLIST
    if [[ "$GREP" != "" ]]; then
        _MYBASH_TEST_TESTLIST=$(ls $DIR/**/*.spec.sh | grep $GREP)
    else
        _MYBASH_TEST_TESTLIST=$(ls $DIR/**/*.spec.sh)
    fi

    local _MYBASH_TEST_TEST
    local _MYBASH_TEST_TEST_ABS
    _MYBASH_TEST_ISFIRSTRUN="1"
    for _MYBASH_TEST_TEST in $_MYBASH_TEST_TESTLIST; do
        _MYBASH_TEST_TEST_ABS=$(cd $(dirname $_MYBASH_TEST_TEST); pwd)/$(basename $_MYBASH_TEST_TEST)
        $_MYBASH_TEST_TEST
    done

    # _MYBASH_TEST_ISFIRSTRUN="0"
    # for _MYBASH_TEST_TEST in $_MYBASH_TEST_TESTLIST; do
    #     ./$_MYBASH_TEST_TEST
    # done

    myBash.test._printStatus
}
declare -fr myBash.test.main

myBash.test.main "$@"


# EOF
