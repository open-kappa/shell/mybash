# MYBASH

A set of bash scripting utilities

## HOWTO

Each script is documented.
To load the, choose one of the following:

* Just copy the specific script, and source it inside your script:

```bash
cp mybash/scripts/core/echo.sh .
. echo.sh
```

* Source `myBash.sh` and then call the myBash() method to get all the content

```bash
. mybash/scripts/myBash.sh
myBash
```

* Source the specific "package" `main.sh`, and then call the relative `source`:

```bash
. mybash/scripts/core/main.sh
sourceMyBashCore
```

## License

MIT. @copyright Francesco Stefanni
